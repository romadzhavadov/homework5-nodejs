import fs from 'fs';

interface NewsPost {
  id: number;
  title: string;
  text: string;
  createDate: Date;
}

class FileDB<T extends NewsPost> {
  private schemas: Record<string, T> = {};

  registerSchema(name: string, schema: T): void {
    this.schemas[name] = schema;
  }

  getTable(name: string): Table<T> {
    const schema = this.schemas[name];
    if (!schema) {
      throw new Error(`Schema '${name}' is not registered.`);
    }

    return new TableImpl<T>(`${name}.json`, schema);
  }
}

interface Table<T extends NewsPost> {
  getAll(): T[];
  getById(id: number): T | undefined;
  create(item: Partial<T>): T;
  update(id: number, updates: Partial<T>): T;
  delete(id: number): void;
}

class TableImpl<T extends NewsPost> implements Table<T> {
  constructor(private filename: string, private schema: T) {}

  private _readData(): T[] {
    try {
      const rawData = fs.readFileSync(this.filename, 'utf-8');
      return JSON.parse(rawData);
    } catch (error) {
      console.error(error);
      return [];
    }
  }

  private _writeData(data: T[]): void {
    fs.writeFileSync(this.filename, JSON.stringify(data, null, 2));
  }

  getAll(): T[] {
    const data = this._readData();
    return data;
  }

  getById(id: number): T | undefined {
    const data = this._readData();
    return data.find((item: T) => item.id === id);
  }

  create(item: T): T {
    const data = this._readData();
    const newItem = { ...item, id: Date.now() } as T;
    data.push(newItem);
    this._writeData(data);
    return newItem;
  }

  update(id: number, updates: Partial<T>): T {
    const data = this._readData();
    const index = data.findIndex((item: T) => item.id === id);
    if (index === -1) {
      throw new Error(`Item with id ${id} not found.`);
    }

    const updatedItem = { ...data[index], ...updates } as T;
    data[index] = updatedItem;
    this._writeData(data);
    return updatedItem;
  }

  delete(id: number): void {
    const data = this._readData();
    const index = data.findIndex((item: T) => item.id === id);
    if (index === -1) {
      throw new Error(`Item with id ${id} not found.`);
    }

    data.splice(index, 1);
    this._writeData(data);
  }
}





const filename = 'FileDB';
const db = new FileDB<NewsPost>();
const newspostSchema: NewsPost = {
  id: 1,
  title: 'Title',
  text: "Text",
  createDate: new Date(),
};

// Реєстрація схеми та отримання таблиці
db.registerSchema(filename, newspostSchema);
const newspostTable = db.getTable(filename);

// Використання методів таблиці
const newItem = newspostTable.create({ title: 'Item 1', text: 'item 1 text' });
console.log('New item created:', newItem);

const allItems = newspostTable.getAll();
console.log('All items:', allItems);

const itemId = newItem.id;
const foundItem = newspostTable.getById(itemId);
console.log('Found item by id:', foundItem);

const updatedItem = newspostTable.update(itemId, { title: 'Updated Item' });
console.log('Updated item:', updatedItem);

newspostTable.delete(itemId);
console.log('Item deleted successfully.');



// Використано Generics для загальні типів класу FileDB, 
// це дозволяє створювати універсальний клас, який може працювати з різним типом даних, що наслідується від NewsPost. 
